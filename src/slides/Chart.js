import React from "react";
import mermaid from "mermaid/dist/mermaid";

function Slide() {
  const graph = `
    graph TB
    Client
    Component
    Layout
    Manipulators
    Plugins
    Profiles
    Renderer
    
    Client---Profiles
    Profiles---Plugins
    Profiles---Layout
    Plugins---Component
    Plugins---Manipulators
    Layout---Renderer
  `;

  mermaid.init();

  return (
    <div
      className="mermaid"
      style={{
        margin: "auto",
        textAlign: "center",
        width: "80vw"
      }}
    >
      {graph}
    </div>
  );
}

export default Slide;
