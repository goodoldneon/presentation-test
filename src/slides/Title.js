import React from "react";
import { Heading, Slide, Text } from "spectacle";

function Title() {
  return (
    <Slide transition={["fade"]} bgColor="#cfeef7">
      <Heading size={1} fit caps lineHeight={1} textColor="secondary">
        Unity Web
      </Heading>
    </Slide>
  );
}

export default Title;
